package client

import common.Keyword

/**
 * Receives messages from the [server.Server] and prints them to the standard output.
 *
 * If receives [disconnect][Keyword.DISCONNECT] command from the server, tells [context] to disconnect.
 *
 * @param context the [Client] which want to receive messages
 * */
class Listener(private val context: Client) : Thread(){

    override fun run() {
        println("Listening on ${context.socket}")
        while (true) {

            val message = context.receive()
            if (message.indexOf(Keyword.DISCONNECT) == 0) {
                break
            } else {
                println(message)
            }
        }
        context.disconnect()
    }
}