package client

import common.Keyword
import java.io.DataInputStream
import java.io.DataOutputStream
import java.net.Socket

/**
 * Starts client session, sends messages from the standard input.
 *
 * @constructor Initiates connection with the [server.Server]
 * @param ip [server.Server] IP address
 * @param port [server.Server] port
 * @property socket connection to the server
 * @property listener paired [Listener] object
 *
 * @see [server.Server]
 */
open class Client(ip: String, port: Int) {
    val socket                  = Socket(ip, port)
    private lateinit var listener: Listener
    private val outputStream    = DataOutputStream(socket.getOutputStream())
    private val inputStream     = DataInputStream(socket.getInputStream())

    /**
     * Waits for the user input and calls [send] for each line.
     * */
    fun start() {
        listener = Listener(this)
        listener.start()
        send(Keyword.HELLO)

        while (true) {
            val message = readLine()?.trim()
            if (message == "" || message == null) continue
            send(message)
        }
    }

    /**
     * Receives message from the [server.Server]
     * */
    fun receive(): String = inputStream.readUTF()

    /**
     * Sends message to the [server.Server]
     * */
    fun send(msg: String) = outputStream.writeUTF(msg)

    /**
     * Tells the [server.Server] about intent to disconnect and breaks down the connection.
     * */
    fun disconnect() {
        send(Keyword.DISCONNECT)
        socket.close()
    }
}