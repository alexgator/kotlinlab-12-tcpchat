package server

import common.Keyword
import common.User
import java.net.*
import kotlin.concurrent.thread

/**
 * Accepts connections, manages user information, message delivery.
 *
 * @param port port to receive connections
 * @param logging log output switch
 * @property socket TCP socket which receives connections
 * @property users set of connected users
 *
 * @see common.Keyword
 * @see common.User
 * */
class Server(port: Int, private val logging: Boolean = false) : Thread() {

    private val socket      = ServerSocket(port)
    var users               = emptySet<User>()


    /**
     * Starts TCP socket, runs new thread for each incoming connection.
     *
     * The incoming information may be just a message or a command.
     * Simple message will be sent to all connected users except
     * the sender by default.
     * Command begins from a symbol '@' and contains a keyword and an argument.
     * Keywords are defined in [common.Keyword].
     * */
    override fun run() {
        val bot = Bot(this)
        Thread(bot).start()
        while (true) {
            val connectionSocket = socket.accept()

            thread(start = true) {
                loop@while (true) {
                    val user = findOrAddUser(connectionSocket)

                    val message = user.receive()

                    if (logging) logInfo("Incoming message from $user:\n$message")

                    if (Regex("^@\\w*").find(message) != null) {
                        // It is command
                        val command = Regex("@\\w*").find(message)?.value
                        val argument = message.replace(Regex("^@\\w*\\s*"), "")
                        if (logging) logInfo("Command from $user:\n$command | $argument\n")

                        when (command) {
                            Keyword.HELLO       -> sayHiTo(user)
                            Keyword.NAME        -> trySetNameTo(user, argument)
                            Keyword.SENDUSER    -> sendDirect(user, argument)
                            Keyword.QUIT        -> {
                                sayByeTo(user)
                                break@loop
                            }
                            else            -> user.send("Unknown command.")
                        }
                    } else {
                        // Just a message
                        broadcast("${user.name}:\n$message", user)
                    }
                }
            }
        }
    }

    /**
     * Searches for the [connection] in [users], and if [connection] is not found, creates
     * a new [User] object and inserts it into [users].
     *
     * @return A [User] object for provided connection socket
     * */
    private fun findOrAddUser(connection: Socket): User {
        var user: User? = users.find { it.connection == connection }

        if (user == null) {
            if (logging) logInfo("User connected: ${connection.inetAddress}")
            user = User(connection = connection)
            users = users.plusElement(user)
            if (logging) logInfo("Connected users: $users")
        }

        return user
    }

    private fun sendDirect(user: User, argument: String) {
        val firstQuoteIndex = argument.indexOf('\"')
        val lastQuoteIndex = argument.lastIndexOf('\"')

        val recipientName = argument.substring(firstQuoteIndex + 1, lastQuoteIndex).trim()
        val messageText   = argument.substring(lastQuoteIndex + 1, argument.length).trim()

        val recipient = users.find { it.name == recipientName }

        if (logging) logInfo("Direct message to $recipientName: $messageText")

        try {
            when {
                recipientName == "" -> throw Exception("Recipient name is empty.")
                messageText == ""   -> return
                recipient == null   -> throw Exception("User \'$recipientName\' is not found.")
            }

            recipient?.send("Direct message from ${user.name}:\n$messageText")
        } catch (e: Exception) {
            user.send("Error: ${e.message}")
        }
    }

    /**
     * Breaks socket connection and removes [user] from connected [users].
     * */
    private fun disconnect(user: User) {
        users = users.minus(user)
        user.disconnect()
        if (logging) logInfo("User disconnected: ${user.ip}:${user.port}")
    }

    /**
     * Sends [message] for every user except the [sender].
     * */
    fun broadcast(message: String, sender: User) {
        users.filter { it != sender }
                .forEach { it.send(message) }
    }

    /**
     * Sends the welcome message and short manual to the [user].
     *
     * Notifies other users about [newcomer][user].
     * */
    private fun sayHiTo(user: User) {
        val msg = "Welcome to the chat, ${user.name}\n" +
                "Here is a list of available commands:\n" +
                "\t@hello\tdisplay this message\n" +
                "\t@name\tset your name\n" +
                "\t@quit\texit from the chat\n"
        user.send(msg)
        broadcast("${user.name} joined to the chat.", user)
    }

    /**
     * Sends the farewell message to the [user] and disconnects him.
     * Notifies other users about it.
     * */
    private fun sayByeTo(user: User) {
        user.send("Bye!")
        broadcast("${user.name} left the chat.", user)
        disconnect(user)
    }

    /**
     * Sets [nickname] to the [user] if it is correct and not occupied by another user.
     * */
    private fun trySetNameTo(user: User, nickname: String) {
        when {
            nickname.trim() == "" -> {
                user.send("Failed to set nickname. Incorrect nickname.")
            }
            users.any { it.name == nickname } -> {
                user.send("Failed to set nickname \'$nickname\'. Nickname already taken.")
            }
            else -> {
                broadcast("User \'${user.name}\' has changed his nickname to \'$nickname\'", user)
                user.name = nickname
                user.send("Success. Now your nickname is $nickname.")
            }
        }
    }

    /**
     * Outputs [message] with INFO prefix.
     * */
    private fun logInfo(message: String) {
        println("INFO: $message")
    }
}