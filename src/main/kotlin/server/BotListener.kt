package server

import common.Keyword

class BotListener(private val context: Bot): Thread() {

    companion object {
        private val TRIGGER_SEQ = "привет"
        private val TRIGGER_MSG = "Дратути"
    }

    override fun run() {
        println("Bot listening on ${context.socket}")
        while (true) {
            val message = context.receive()
            if (message.indexOf(Keyword.DISCONNECT) == 0) {
                break
            }
            if (message.toLowerCase().contains(TRIGGER_SEQ)) {
                context.send("$TRIGGER_MSG, ${message.lines()[0].substringBefore(':')}!")
            }
        }
        context.disconnect()
    }
}