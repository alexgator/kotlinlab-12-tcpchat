package server

import client.Client
import java.lang.Thread.sleep
import java.text.DateFormat
import java.util.*

class Bot(private val context: Server) : Client("localhost", 1337), Runnable {
    private var listener = BotListener(this)

    override fun run() {
        send("@name Bot")
        listener.start()
        while (true) {
            send("${DateFormat.getTimeInstance().format(Date())}\n" +
                    "${context.users.size} connected.")
            sleep(60000)
        }
    }

    fun stop() {
        disconnect()
        Thread.currentThread().interrupt()
    }

}