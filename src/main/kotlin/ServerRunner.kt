import server.Bot
import server.Server

fun main(args: Array<String>) {
    val srv = Server(1337, true)
    srv.start()
}