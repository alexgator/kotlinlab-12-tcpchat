package common

import java.io.DataInputStream
import java.io.DataOutputStream
import java.net.Socket

/**
 * Presents connected user. Contains methods to send and receive messages.
 *
 * @param name name of the user
 * @param connection connection socket
 * @property ip connection IP address
 * @property port connection port
 *
 * @see [server.Server]
 * */
class User (var name: String = "Anonymous user", val connection: Socket) {
    private val inputStream = DataInputStream(connection.getInputStream())
    private val outputStream = DataOutputStream(connection.getOutputStream())

    val ip = connection.inetAddress!!
    val port = connection.port

    /**
     * Receive a line of text from the user.
     * */
    fun receive(): String = inputStream.readUTF()

    /**
     * Send the [message] to the user.
     * */
    fun send(message: String) = outputStream.writeUTF(message)

    /**
     * Close connection with the user.
     * */
    fun disconnect() = connection.close()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as User

        if (name != other.name) return false
        if (connection != other.connection) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + connection.hashCode()
        return result
    }

    override fun toString(): String {
        return "$name(ip=$ip, port=$port)"
    }
}