package common

/**
 * Keywords of commands
 * */
abstract class Keyword {
    companion object {
        val HELLO       = "@hello"
        val NAME        = "@name"
        val QUIT        = "@quit"
        val DISCONNECT  = "@bye"
        val SENDUSER    = "@senduser"
    }
}
